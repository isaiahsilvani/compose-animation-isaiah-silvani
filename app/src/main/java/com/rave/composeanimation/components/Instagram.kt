/**
 * Created by Jimmy McBride on 2022-12-05
 *
 * Copyright © 2022 Jimmy McBride
 */
package com.rave.composeanimation.components

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay

/**
 * Instagram
 *
 * @author Jimmy McBride on 2022-12-05.
 */
@Composable
fun Instagram(
    size: Int,
    triggerAnimation: Boolean
) {
    var currentPhase by remember {
        mutableStateOf(ColorPhase.FIRST)
    }

    /** Colors will change based the current phase of the cycle we're in **/
    val firstColor by animateColorAsState(
        targetValue = getFirstColorByPhase(currentPhase)
    )
    val secondColor by animateColorAsState(
        targetValue = getSecondColorByPhase(currentPhase)
    )
    val thirdColor by animateColorAsState(
        targetValue = getThirdColorByPhase(currentPhase)
    )
    /** Handles moving to the next phase **/
    LaunchedEffect(key1 = triggerAnimation) {
        if (triggerAnimation) {
            while (true) {
                delay(100L)
                currentPhase = getNextPhase(currentPhase)
            }
        }
    }

    Canvas(modifier = Modifier
        .size(size.dp)
        .padding(16.dp),
        onDraw = {
            drawRoundRect(
                brush = Brush.linearGradient(listOf(firstColor, secondColor, thirdColor)),
                cornerRadius = CornerRadius(60f, 60f),
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(listOf(firstColor, secondColor, thirdColor)),
                radius = 45f,
                style = Stroke(width = 15f, cap = StrokeCap.Round)
            )
            drawCircle(
                brush = Brush.linearGradient(listOf(firstColor, secondColor, thirdColor)),
                radius = 13f,
                center = Offset(this.size.width * .80f, this.size.height * 0.20f),
            )
        }
    )
}
