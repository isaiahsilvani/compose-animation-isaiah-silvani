package com.rave.composeanimation.components

import androidx.compose.animation.*
import androidx.compose.animation.core.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.size
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.rave.composeanimation.R

@Composable
fun DropDown() {
    var toggleDropDown by remember {
        mutableStateOf(false)
    }
    val transition = updateTransition(targetState = toggleDropDown, label = "Drop Down Size Transition")
    val iconSize by transition.animateDp(label = "Animate Icon Size") { state ->
        when (state) {
            true -> 400.dp
            false -> 100.dp
        }
    }
    Button(onClick = {
        toggleDropDown = !toggleDropDown
    }) {
        Text(text = if (toggleDropDown) "REVEAL ICON" else "SHOW ICON")
    }
    AnimatedVisibility(
        visible = toggleDropDown,
        enter = slideInVertically(
            animationSpec = tween(1000,
                easing = CubicBezierEasing(0.4f, 0.0f, 1.0f, 1.0f))
        ),
        exit = slideOutVertically(
            animationSpec = tween(1000, easing = CubicBezierEasing(0.4f, 0.0f, 1.0f, 1.0f)),
        ),
    ) {
        Image(
            painter = painterResource(id = R.drawable.rickandmorty),
            modifier = Modifier.size(iconSize),
            contentDescription = "Rick and morty Logo"
        )
    }
}