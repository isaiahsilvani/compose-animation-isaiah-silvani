package com.rave.composeanimation.components

import androidx.compose.ui.graphics.Color


enum class ColorPhase {
    FIRST, SECOND, THIRD
}

fun getFirstColorByPhase(phase: ColorPhase): Color {
    return when (phase) {
        ColorPhase.FIRST -> Color.Red
        ColorPhase.SECOND -> Color.Magenta
        ColorPhase.THIRD -> Color.Yellow
    }
}

fun getSecondColorByPhase(phase: ColorPhase): Color {
    return when (phase) {
        ColorPhase.FIRST -> Color.Magenta
        ColorPhase.SECOND -> Color.Yellow
        ColorPhase.THIRD -> Color.Red
    }
}

fun getThirdColorByPhase(phase: ColorPhase): Color {
    return when (phase) {
        ColorPhase.FIRST -> Color.Yellow
        ColorPhase.SECOND -> Color.Red
        ColorPhase.THIRD -> Color.Magenta
    }
}

fun getNextPhase(phase: ColorPhase): ColorPhase {
    return when (phase) {
        ColorPhase.FIRST -> {
            ColorPhase.SECOND
        }
        ColorPhase.SECOND -> {
            ColorPhase.THIRD
        }
        ColorPhase.THIRD -> {
            ColorPhase.FIRST
        }
    }
}
//First color should rotate colors as follow: Yellow, Red, Magenta
//Second color should rotate colors as follow: Red, Magenta, Yellow
//Third color should rotate colors as follow: Magenta, Yellow, Red

// 1st color phases, Yellow, Red, Magenta
// 2nd color phases, Red, Magenta, Yellow
// 3rd color phases, Magenta, Yellow, Red