package com.rave.composeanimation

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.rave.composeanimation.components.DropDown
import com.rave.composeanimation.components.Greeting
import com.rave.composeanimation.components.Instagram
import com.rave.composeanimation.ui.theme.ComposeAnimationTheme
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeAnimationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var triggerAnimation by remember {
                        mutableStateOf(false)
                    }
                    var instagramVisibility by remember {
                        mutableStateOf(false)
                    }
                    // When triggerAnimation changes, this block will be run
                    LaunchedEffect(key1 = triggerAnimation) {
                        Log.d("AYE", "onCreate: animation triggered")
                    }
                    val scope = rememberCoroutineScope()
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center,
                    ) {
                        Button(onClick = {
                            instagramVisibility = !instagramVisibility
                        }) {
                            Greeting("RaveBizz")
                        }
                        AnimatedVisibility(
                            visible = instagramVisibility,
                            enter = slideInVertically() + fadeIn(),
                            exit = slideOutHorizontally()
                        ) {
                            IconButton(
                                onClick = {
                                    scope.launch {
                                        triggerAnimation = !triggerAnimation
                                    }
                                },
                            ) {
                                Instagram(size = 100, triggerAnimation)
                            }
                        }
                        Spacer(modifier = Modifier.height(15.dp))
                        DropDown()
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeAnimationTheme {
        Greeting("Android")
    }
}
